import model.automaton_factory

__author__ = 'Violette'

import json

import model.state
import model.field
import model.automaton
import view.simple_console_viewer
import file_parser.automaton_parser

file = open("automaton-4.json")
file2 = open("automaton-5.json")
automaton_parser = file_parser.automaton_parser.Automaton_Parser()
json_dict = json.loads(file.read())
json_dict2 = json.loads(file2.read())
automaton = automaton_parser.Parse(json_dict["NFA"], model.automaton.AutomatonType.NFA)
automaton2 = automaton_parser.Parse(json_dict2["DFA"], model.automaton.AutomatonType.DFA)
factory = model.automaton_factory.AutomatonFactory()
automaton = factory.Intersection(automaton, automaton2)
automaton.Next("0")
file.close()
viewer = view.simple_console_viewer.ConsoleViewer(automaton)
viewer.ShowAutomaton()
if automaton.Accepting():
    print("Intersection test failed. NFA shouldn't accept entry.")
    exit(1)
print()
file = open("automaton-4.json")
file2 = open("automaton-5.json")
automaton_parser = file_parser.automaton_parser.Automaton_Parser()
json_dict = json.loads(file.read())
json_dict2 = json.loads(file2.read())
automaton = automaton_parser.Parse(json_dict["NFA"], model.automaton.AutomatonType.NFA)
automaton2 = automaton_parser.Parse(json_dict2["DFA"], model.automaton.AutomatonType.NFA)
factory = model.automaton_factory.AutomatonFactory()
automaton = factory.Union(automaton, automaton2)
automaton.Next("0")
file.close()
viewer = view.simple_console_viewer.ConsoleViewer(automaton)
viewer.ShowAutomaton()
if not automaton.Accepting():
    print("Union test failed. NFA should accept entry.")
    exit(1)
