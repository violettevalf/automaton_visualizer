__author__ = 'Violette'

import json

import file_parser.automaton_parser
import model.automaton
import view.simple_console_viewer

file = open("automaton-2.json")
file2 = open("automaton-3.json")
automaton_parser = file_parser.automaton_parser.Automaton_Parser()
json_dict = json.loads(file.read())
json_dict2 = json.loads(file2.read())
automaton = automaton_parser.Parse(json_dict["DFA"], model.automaton.AutomatonType.DFA)
automaton2 = automaton_parser.Parse(json_dict2["DFA"], model.automaton.AutomatonType.DFA)
factory = model.automaton_factory.AutomatonFactory()
automaton = factory.Intersection(automaton, automaton2)
automaton.Next("0")
automaton.Next("1")
automaton.Next("0")
file.close()
viewer = view.simple_console_viewer.ConsoleViewer(automaton)
viewer.ShowAutomaton()
if automaton.Accepting():
    print("Intersection test failed. DFA shouldn't accept entry.")
    exit(1)
print()
file = open("automaton-2.json")
file2 = open("automaton-3.json")
automaton_parser = file_parser.automaton_parser.Automaton_Parser()
json_dict = json.loads(file.read())
json_dict2 = json.loads(file2.read())
automaton = automaton_parser.Parse(json_dict["DFA"], model.automaton.AutomatonType.DFA)
automaton2 = automaton_parser.Parse(json_dict2["DFA"], model.automaton.AutomatonType.DFA)
factory = model.automaton_factory.AutomatonFactory()
automaton = factory.Union(automaton, automaton2)
automaton.Next("0")
automaton.Next("1")
automaton.Next("0")
file.close()
viewer = view.simple_console_viewer.ConsoleViewer(automaton)
viewer.ShowAutomaton()
if not automaton.Accepting():
    print("Union test failed. DFA should accept entry.")
    exit(1)
