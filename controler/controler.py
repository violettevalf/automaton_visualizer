import functools

import PyQt5.QtCore

import model.automaton_factory
import model.automaton
from aux_functions import auxilary_functions
import aux_functions.emptiness_checker

class Controller():
    def __init__(self, graph_widget, automaton_description, accept_descr, stack_view, step_by_step_button, state_descr,
                 stack_num, reorder_fun, add_new_automaton_fun):
        self.automaton_ = None
        self.nodes_ = set()
        self.edges = {}
        self.stacks_ = {}
        self.states_to_show_list_ = []
        self.graph_widget_ = graph_widget
        self.automaton_factory = model.automaton_factory.AutomatonFactory()
        self.automaton_description_ = automaton_description
        self.accept_descr = accept_descr
        self.stack_view_ = stack_view
        self.step_by_step_button_ = step_by_step_button
        self.reorder_fun_ = reorder_fun
        self.add_new_automaton_fun_ = add_new_automaton_fun
        self.continue_run_ = False
        self.infinity_sequence_start_symbol_ = ""
        self.state_descr_ = state_descr
        self.stack_num_ = stack_num
        self.stack_curr_number = 0
        self.graph_widget_.AddSelectionChangedFunc(self.NodesSelectionChanged)

    def ChangeInfinitySeqStartSymbol(self, symbol):
        self.infinity_sequence_start_symbol_ = symbol

    def ChangeAutomaton(self, automaton):
        self.automaton_ = automaton
        self.nodes_ = self.automaton_.states_
        for node in self.nodes_:
            self.edges[node] = set()
        self.graph_widget_.Clear()
        self.ShowAutomaton()

    def MakeOneStep(self, letter):
        if letter not in self.automaton_.alphabet_:
            return
        self.automaton_.Next(letter)
        self.UpdateView()

    def __SetAcceptDescription__(self):
        checker = aux_functions.emptiness_checker.EmptinessChecker(self.nodes_, self.edges,
                                                                   self.automaton_.accept_states_)
        text = self.automaton_.AcceptDescription() + "\n"
        if self.automaton_.type_ != model.automaton.AutomatonType.PDA:
            text += "Is automaton empty: " + str(checker.Emptiness(self.automaton_.initial_state_)) + "\n"
            text += "Is Buchi empty: " + str(checker.BuchiEmptiness(self.automaton_.initial_state_))
        self.accept_descr.setText(text)

    def UpdateView(self):
        self.stacks_ = {}
        for state in self.automaton_.states_:
            self.graph_widget_.UpdateCurrent(state, self.automaton_.IsCurrent(state))
            stacks = self.automaton_.GetStack(state)
            if stacks:
                self.stacks_[state] = stacks
        self.states_to_show_list_ = list(self.stacks_)
        self.automaton_description_.setText(str(self.automaton_))
        self.__SetAcceptDescription__()
        self.CreateStack()

    def __Run__(self, word, latency):
        if self.continue_run_ and word:
            self.MakeOneStep(word[0])
            if len(word) == 1:
                self.continue_run_ = False
                self.step_by_step_button_.setText("Step by step run")
            else:
                PyQt5.QtCore.QTimer.singleShot(latency, functools.partial(self.__Run__, word[1:], latency))
        else:
            self.continue_run_ = False

    def __BuchiRun__(self, word, latency):
        if word[0] == self.infinity_sequence_start_symbol_:
            word = word[1:] + word
        if self.continue_run_ and word:
            self.MakeOneStep(word[0])
            if len(word) == 1:
                self.continue_run_ = False
                self.step_by_step_button_.setText("Step by step run")
            else:
                PyQt5.QtCore.QTimer.singleShot(latency, functools.partial(self.__BuchiRun__, word[1:], latency))
        else:
            self.continue_run_ = False

    def Run(self, word):
        if self.infinity_sequence_start_symbol_ and self.infinity_sequence_start_symbol_ in word:
            states = self.automaton_.transition_fn_.BuchiRun(self.automaton_.current_state_, word,
                                                          self.infinity_sequence_start_symbol_)
            self.automaton_.current_state_ = states
            self.UpdateView()
            return
        for letter in word:
            self.MakeOneStep(letter)

    def RunStepByStep(self, word):
        if self.continue_run_:
            self.continue_run_ = False
            self.step_by_step_button_.setText("Step by step run")
        else:
            self.step_by_step_button_.setText("Stop")
            self.continue_run_ = True
            if self.infinity_sequence_start_symbol_ and self.infinity_sequence_start_symbol_ in word:
                self.__BuchiRun__(word, 500)
            else:
                self.__Run__(word, 500)

    def ShowAutomaton(self):
        for state in self.automaton_.states_:
            self.graph_widget_.AddNode(state, self.automaton_.IsAccepting(state),
                                       self.automaton_.IsCurrent(state), state.GetDescription())
        for state in self.automaton_.states_:
            letter_to_list = auxilary_functions.GetEdges(self.automaton_, state)
            target_state_to_letters = {}
            for letter in letter_to_list:
                for target_state in letter_to_list[letter]:
                    #TODO(violette): check it
                    if not target_state in target_state_to_letters:
                        target_state_to_letters[target_state] = []
                    target_state_to_letters[target_state] += [letter]
            for target_state in target_state_to_letters:
                text = target_state_to_letters[target_state]
                if len(text) == 1:
                    text = text[0]
                self.graph_widget_.AddEdge(state, target_state, auxilary_functions.ParseList((text)))
                self.edges[state].add(target_state)
        self.reorder_fun_()
        self.UpdateView()


    def CanJoinWith(self, automaton):
        return self.automaton_.alphabet_ == automaton.alphabet_

    def IntersectionWith(self, automaton):
        automaton = self.automaton_factory.Intersection(self.automaton_, automaton)
        self.add_new_automaton_fun_(automaton)
        self.ChangeAutomaton(automaton)

    def UnionWith(self, automaton):
        automaton = self.automaton_factory.Union(self.automaton_, automaton)
        self.add_new_automaton_fun_(automaton)
        self.ChangeAutomaton(automaton)

    def Reset(self):
        self.automaton_.Reset()
        for state in self.automaton_.states_:
            self.graph_widget_.UpdateCurrent(state, self.automaton_.IsCurrent(state))
        self.UpdateView()

    def NodesSelectionChanged(self):
        states = self.graph_widget_.GetSelectedStates()
        if not states:
            if self.automaton_:
                states = list(self.automaton_.GetCurrent())
        self.states_to_show_list_ = states
        self.CreateStack()

    def CreateStack(self):
        if not self.stacks_:
            return
        self.stacks_number_ = 0
        to_delete = []
        for state in self.states_to_show_list_:
            if not state in self.stacks_:
                to_delete.append(state)
                continue
            self.stacks_number_ += len(self.stacks_[state])
        for state in to_delete:
            self.states_to_show_list_.remove(state)
        if self.stacks_number_ > 0:
            self.ShowStack(0)


    def ShowStack(self, num):
        def aux():
            pos = 0
            summed = 0
            for state in self.states_to_show_list_:
                state_len = len(self.stacks_[state])
                if summed + state_len <= num:
                    pos += 1
                    summed += state_len
                    continue
                return pos, num - summed

        state_pos, stack_pos = aux()
        self.stack_view_.clear()
        self.stack_curr_number = num
        state = self.states_to_show_list_[state_pos]
        stack = self.stacks_[state][stack_pos]
        if self.stacks_:
            for elem in reversed(stack):
                self.stack_view_.addItem(elem)
        self.state_descr_.setText(state.GetDescription())
        self.stack_num_.setText(str(num + 1) + " / " + str(self.stacks_number_))

    def NextStack(self):
        if self.stacks_number_ > 0:
            self.stack_curr_number += 1
            self.ShowStack(self.stack_curr_number % self.stacks_number_)

    def PreviousStack(self):
        if self.stacks_number_ > 0:
            self.stack_curr_number = self.stack_curr_number - 1 + self.stacks_number_
            self.ShowStack(self.stack_curr_number % self.stacks_number_)
