#!/bin/bash

# Debian based distributions can use this script
# to install the required dependencies

# Install python3 and pip3
sudo apt install python3 python3-pip

# Install PyQt5
sudo apt-get install python3-pyqt5

# Install numpy
pip3 install numpy

# Install NetworkX
pip3 install networkx

# Install Graphviz
sudo apt install graphviz libgraphviz-dev

# Download PyGraphviz
pip3 install pygraphviz
# sudo pip3 install git+git://github.com/pygraphviz/pygraphviz.git # This method should be used if certaing layouts are not working (there are some problems with python3 binding)