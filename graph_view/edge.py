import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets
import graph_view.node
import graph_view.painting_supplier


class Edge(PyQt5.QtWidgets.QGraphicsPathItem):
    class SelfEdge:
        def __init__(self, edge):
            self.edge_ = edge

        def Adjust(self):
            control_source = PyQt5.QtCore.QPointF(self.edge_.source_pos_.x() - 45,
                                     self.edge_.source_pos_.y() - 100)
            control_target = PyQt5.QtCore.QPointF(self.edge_.target_pos_.x() + 45,
                                     self.edge_.target_pos_.y() - 100)
            self.__SetPathFromPoints__(self.edge_.source_pos_, control_source, control_target,
                                       self.edge_.target_pos_)

        def __SetPathFromPoints__(self, source_calculated_pos, control_source, control_target, target_calculated_pos):
            path = PyQt5.QtGui.QPainterPath()
            path.moveTo(source_calculated_pos)
            path.cubicTo(control_source, control_target, target_calculated_pos)
            self.edge_.setPath(path)

    class SingleEdge:
        def __init__(self, edge):
            self.edge_ = edge

        def Adjust(self):
            self.__SetPathFromPoints__(self.edge_.source_pos_, self.edge_.target_pos_)

        def __SetPathFromPoints__(self, source_calculated_pos, target_calculated_pos):
            path = PyQt5.QtGui.QPainterPath()
            path.moveTo(source_calculated_pos)
            path.lineTo(target_calculated_pos)
            self.edge_.setPath(path)

    class PairedEdge:
        def __init__(self, edge):
            self.edge_ = edge

        def Adjust(self):
            shift = 40
            if self.edge_.is_first_:
                shift = -40
            control_source = PyQt5.QtCore.QPointF(self.edge_.source_pos_.x() + shift,
                                     self.edge_.source_pos_.y() + shift)
            control_target = PyQt5.QtCore.QPointF(self.edge_.target_pos_.x() + shift,
                                     self.edge_.target_pos_.y() + shift)
            self.__SetPathFromPoints__(self.edge_.source_pos_, control_source, control_target,
                                       self.edge_.target_pos_)

        def __SetPathFromPoints__(self, source_calculated_pos, control_source, control_target, target_calculated_pos):
            path = PyQt5.QtGui.QPainterPath()
            path.moveTo(source_calculated_pos)
            path.cubicTo(control_source, control_target, target_calculated_pos)
            self.edge_.setPath(path)

    def __init__(self, source, target, paired=False, label=""):
        super(Edge, self).__init__()
        self.setAcceptHoverEvents(True)
        self.text_position_ = PyQt5.QtCore.QPointF()

        self.source_ = source
        self.target_ = target
        self.paired_ = paired
        self.edge_ = Edge.SingleEdge(self)
        if self.source_ == self.target_:
            self.edge_ = Edge.SelfEdge(self)
        if self.paired_:
            self.edge_ = Edge.PairedEdge(self)

        self.label_ = label
        self.is_first_ = paired
        self.source_pos_, self.target_pos_ = None, None
        self.__UpdatePositions__()

        self.Adjust()

    def SetAsPaired(self):
        self.paired_ = True
        self.edge_ = Edge.PairedEdge(self)
        self.Adjust()

    def __UpdatePositions__(self):
        self.source_pos_ = self.source_.mapToScene(self.source_.boundingRect().center())
        self.target_pos_ = self.target_.mapToScene(self.target_.boundingRect().center())

    def __RecalculateTextPosition__(self):
        if self.label_ == "":
            return
        path = self.path()
        text_metrics = PyQt5.QtGui.QFontMetrics(PyQt5.QtGui.QFont())
        point = path.pointAtPercent(0.5)
        self.text_position_ = PyQt5.QtCore.QPointF(point.x() + 5 - text_metrics.width(
            self.label_) / 2, point.y() + 5 + text_metrics.height() / 2)

    def Adjust(self):
        self.__UpdatePositions__()
        self.edge_.Adjust()
        self.__RecalculateTextPosition__()

    def __SetPen__(self, painter):
        painter.setPen(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().edge_pen)

    def __SetBrush__(self, painter):
        painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().edge_brush)

    def __PaintEdgePointer__(self, painter):
        self.__SetPen__(painter)
        self.__SetBrush__(painter)
        length = self.path().length()
        if length == 0 or self.source_ == self.target_:
            return
        percent = graph_view.painting_supplier.FLAG_NODE_SIZE / (2.0 * length)
        if isinstance(self.edge_, Edge.PairedEdge):
            percent = 0.7
        if percent > 1:
            percent = 0
        painter.drawEllipse(self.path().pointAtPercent(1 - percent), 6, 6)

    def __PaintLabel__(self, painter):
        painter.setFont(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().edge_text_font)
        painter.drawText(self.text_position_, self.label_)

    # noinspection PyMethodOverriding
    def paint(self, painter, option, widget):
        self.__SetPen__(painter)
        self.setZValue(-1)
        self.__PaintEdgePointer__(painter)
        self.__PaintLabel__(painter)

        super(Edge, self).paint(painter, option, widget)
