import PyQt5.QtGui

FLAG_NODE_FILL_COLOR = PyQt5.QtGui.QColor(220, 220, 220)
FLAG_NODE_HIGHLIGHT_COLOR = PyQt5.QtGui.QColor(235, 235, 220)
FLAG_NODE_SELECTED_COLOR = PyQt5.QtGui.QColor(255, 255, 210)
FLAG_NODE_CURRENT_COLOR = PyQt5.QtGui.QColor(173, 180, 199)
FLAG_NODE_CURRENT_HIGHLIGHT_COLOR = PyQt5.QtGui.QColor(188, 195, 199)
FLAG_NODE_CURRENT_SELECTED_COLOR = PyQt5.QtGui.QColor(230, 230, 255)
FLAG_EDGE_LINE_COLOR = PyQt5.QtGui.QColor(10, 10, 10)
FLAG_EDGE_THICKNESS = 1
FLAG_NODE_SIZE = 70
FLAG_NODE_ACCEPTING_RING_SIZE = 60
FLAG_EDGE_FONT = "Arial"
FLAG_EDGE_FONT_SIZE = 20

class PaintingSupplier:
    painting_supplier_ = None

    def __init__(self):
        self.black_pen = PyQt5.QtGui.QPen(PyQt5.QtCore.Qt.black, 1)
        self.edge_pen = PyQt5.QtGui.QPen(FLAG_EDGE_LINE_COLOR, FLAG_EDGE_THICKNESS)
        self.node_fill_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_FILL_COLOR)
        self.node_highlight_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_HIGHLIGHT_COLOR)
        self.node_selected_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_SELECTED_COLOR)
        self.node_current_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_CURRENT_COLOR)
        self.node_current_highlight_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_CURRENT_HIGHLIGHT_COLOR)
        self.node_current_selected_color_brush = PyQt5.QtGui.QBrush(FLAG_NODE_CURRENT_SELECTED_COLOR)
        self.edge_brush = PyQt5.QtGui.QBrush(FLAG_EDGE_LINE_COLOR)
        self.edge_text_font = PyQt5.QtGui.QFont(FLAG_EDGE_FONT, FLAG_EDGE_FONT_SIZE)

    @staticmethod
    def GetPaintingSupplier():
        if not PaintingSupplier.painting_supplier_:
            PaintingSupplier.painting_supplier_ = PaintingSupplier()
        return PaintingSupplier.painting_supplier_
