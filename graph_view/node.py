import PyQt5.QtCore
import PyQt5.QtWidgets
import graph_view.painting_supplier
import enum


class Node(PyQt5.QtWidgets.QGraphicsItem):
    class NodeState:
        class Enum(enum.Enum):
            SELECTED = 1
            HIGHLIGHTED = 2
            CURRENT_SELECTED = 3
            CURRENT_HIGHLIGHTED = 4
            CURRENT = 5
            REGULAR = 6

        def __init__(self, node):
            self.node_ = node
            self.current_ = False
            self.highlighted_ = False

        def UpdateCurrent(self, current):
            self.current_ = current

        def UpdateHighlighted(self, highlighted):
            self.highlighted_ = highlighted

        def ReturnState(self):
            if self.node_.isSelected():
                if self.current_:
                    return Node.NodeState.Enum.CURRENT_SELECTED
                return Node.NodeState.Enum.SELECTED
            if self.highlighted_:
                if self.current_:
                    return Node.NodeState.Enum.CURRENT_HIGHLIGHTED
                return Node.NodeState.Enum.HIGHLIGHTED
            if self.current_:
                return Node.NodeState.Enum.CURRENT
            return Node.NodeState.Enum.REGULAR

    def __init__(self, accepting=False, initial=False, tool_tip="lol"):
        super(Node, self).__init__()
        self.setFlag(PyQt5.QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setFlag(PyQt5.QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(PyQt5.QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setAcceptHoverEvents(True)
        self.setToolTip(tool_tip)

        self.state_ = Node.NodeState(self)
        self.state_.UpdateCurrent(initial)
        self.edges_ = []
        self.accepting_ = accepting

    def AddEdge(self, edge):
        if edge not in self.edges_:
            self.edges_ += [edge]

    def UpdateCurrent(self, current):
        self.state_.UpdateCurrent(current)
        self.update()

    def __SetPen__(self, painter):
        painter.setPen(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().black_pen)

    def __SetBrush__(self, painter):
        state = self.state_.ReturnState()
        if state == Node.NodeState.Enum.SELECTED:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_selected_color_brush)
        if state == Node.NodeState.Enum.HIGHLIGHTED:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_highlight_color_brush)
        if state == Node.NodeState.Enum.CURRENT_SELECTED:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_current_selected_color_brush)
        if state == Node.NodeState.Enum.CURRENT_HIGHLIGHTED:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_current_highlight_color_brush)
        if state == Node.NodeState.Enum.CURRENT:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_current_color_brush)
        if state == Node.NodeState.Enum.REGULAR:
            painter.setBrush(graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().node_fill_color_brush)

    def boundingRect(self):
        return PyQt5.QtCore.QRectF(0, 0, graph_view.painting_supplier.FLAG_NODE_SIZE, graph_view.painting_supplier.FLAG_NODE_SIZE)

    # noinspection PyMethodOverriding
    def paint(self, painter, option, widget):
        self.__SetPen__(painter)
        self.__SetBrush__(painter)
        painter.drawEllipse(0, 0, graph_view.painting_supplier.FLAG_NODE_SIZE, graph_view.painting_supplier.FLAG_NODE_SIZE)

        if self.accepting_:
            offset = (graph_view.painting_supplier.FLAG_NODE_SIZE -
                      graph_view.painting_supplier.FLAG_NODE_ACCEPTING_RING_SIZE) / 2
            painter.drawEllipse(offset, offset, graph_view.painting_supplier.FLAG_NODE_ACCEPTING_RING_SIZE,
                                graph_view.painting_supplier.FLAG_NODE_ACCEPTING_RING_SIZE)

    def hoverEnterEvent(self, event):
        self.state_.UpdateHighlighted(True)
        super(Node, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.state_.UpdateHighlighted(False)
        super(Node, self).hoverLeaveEvent(event)

    def itemChange(self, change, value):
        if change == PyQt5.QtWidgets.QGraphicsItem.ItemPositionChange:
            for edge in self.edges_:
                edge.Adjust()
        return super(Node, self).itemChange(change, value)
