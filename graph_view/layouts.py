# TODO(violette): Check if those layouts work. It is not tested yet.
# All those functions should return proper dicts.
import random
import math
import collections

try:
    import networkx as nx
except ImportError:
    raise ImportError("layouts requires networkx")

try:
    from networkx.drawing.nx_agraph import graphviz_layout
except ImportError:
    raise ImportError("layous requires networkx.drawing")

class Layout:
    @staticmethod
    def LayoutsDict():
        layouts_list = [("Grid layout", Layout.grid_layout), ("Circular layout", Layout.circular_layout),
                        ("Random layout", Layout.random_layout),
                        ("Fruchterman Reingold layout", Layout.fruchterman_reingold_layout),
                        ("dot layout", Layout.dot_layout), ("neato layout", Layout.neato_layout),
                        ("fdp layout", Layout.fdp_layout), ("twopi layout", Layout.twopi_layout),
                        ("circo layout", Layout.circo_layout)]
        return collections.OrderedDict(layouts_list)

    @staticmethod
    def create_networkx_graph(nodes, edges):
        try:
            import networkx as nx
        except ImportError:
            raise ImportError("create_networkx_graph requires networkx")
        nx_graph = nx.DiGraph()
        nx_graph.add_nodes_from(nodes)
        nx_graph.add_edges_from(edges)
        return nx_graph

    @staticmethod
    def _rescale_layout(pos):
        if not pos:
            return
        new_pos = {}
        min_x = min(pos.values(), key=lambda x: x[0])[0]
        max_x = max(pos.values(), key=lambda x: x[0])[0]

        min_y = min(pos.values(), key=lambda x: x[1])[1]
        max_y = max(pos.values(), key=lambda x: x[1])[1]

        distance_x = max_x - min_x
        distance_y = max_y - min_y

        if distance_x == 0:
            distance_x = 1

        if distance_y == 0:
            distance_y = 1

        for node_id, node_pos in pos.items():
            node_x = (node_pos[0] - min_x) / distance_x
            node_y = (node_pos[1] - min_y) / distance_y
            new_pos[node_id] = (node_x, node_y)
        return new_pos

    @staticmethod
    def grid_layout(nodes, edges=None):
        pos = {}
        n = len(nodes)
        width = math.ceil(math.sqrt(n))
        step_x = 1.0 / width
        step_y = 1.0 / n * width
        for i, node in enumerate(nodes):
            pos[node] = (i % width * step_x, i // width * step_y)
        return pos

    @staticmethod
    def random_layout(nodes, edges=None):
        pos = {}
        for node in nodes:
            pos[node] = (random.random(), random.random())
        return pos

    @staticmethod
    def circular_layout(nodes, edges=None):
        return Layout._rescale_layout(nx.layout.circular_layout(Layout.create_networkx_graph(nodes, edges)))

    @staticmethod
    def fruchterman_reingold_layout(nodes, edges):
        return Layout._rescale_layout(nx.layout.fruchterman_reingold_layout(Layout.create_networkx_graph(nodes, edges)))

    @staticmethod
    def dot_layout(nodes, edges):
        return Layout._rescale_layout(graphviz_layout(Layout.create_networkx_graph(nodes, edges), prog="dot"))

    @staticmethod
    def neato_layout(nodes, edges):
        return Layout._rescale_layout(graphviz_layout(Layout.create_networkx_graph(nodes, edges), prog="neato"))

    @staticmethod
    def fdp_layout(nodes, edges):
        return Layout._rescale_layout(graphviz_layout(Layout.create_networkx_graph(nodes, edges), prog="fdp"))

    @staticmethod
    def twopi_layout(nodes, edges):
        return Layout._rescale_layout(graphviz_layout(Layout.create_networkx_graph(nodes, edges), prog="twopi"))

    @staticmethod
    def circo_layout(nodes, edges):
        return Layout._rescale_layout(graphviz_layout(Layout.create_networkx_graph(nodes, edges), prog="circo"))