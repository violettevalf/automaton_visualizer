import math

import PyQt5.QtCore
import PyQt5.QtWidgets
import graph_view.painting_supplier

class GridView(PyQt5.QtWidgets.QGraphicsView):
    def __init__(self):
        super(GridView, self).__init__()
        self.setDragMode(PyQt5.QtWidgets.QGraphicsView.RubberBandDrag)

    def __ScaleView__(self, scale_factor):
        factor = self.transform().scale(scale_factor, scale_factor).mapRect(PyQt5.QtCore.QRectF(0, 0, 1, 1)).width()
        if factor < 0.1 or factor > 100:
            return
        factor += 0.9
        self.scale(scale_factor, scale_factor)
        if factor < 1.1:
            factor -= 0.2
        pixel_size = 60/factor
        if pixel_size < 0:
            return
        graph_view.painting_supplier.PaintingSupplier.GetPaintingSupplier().edge_text_font.setPixelSize(pixel_size)

    def wheelEvent(self, event):
        self.__ScaleView__(math.pow(2.0, event.angleDelta().y() / 240.0))
