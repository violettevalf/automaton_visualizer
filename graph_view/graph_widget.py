import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

import graph_view.edge
import graph_view.grid_view
import graph_view.node
import graph_view.painting_supplier


class GraphWidget(PyQt5.QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(GraphWidget, self).__init__(parent)
        self.scene_ = PyQt5.QtWidgets.QGraphicsScene()
        self.view_ = graph_view.grid_view.GridView()
        self.view_.setScene(self.scene_)
        self.view_.setRenderHint(PyQt5.QtGui.QPainter.Antialiasing)
        self.view_.setViewportUpdateMode(PyQt5.QtWidgets.QGraphicsView.FullViewportUpdate)
        layout = PyQt5.QtWidgets.QVBoxLayout()
        layout.addWidget(self.view_)
        self.setLayout(layout)

        self.nodes_dict_ = {}
        self.edges_dict_ = {}

    def Clear(self):
        self.nodes_dict_.clear()
        self.edges_dict_.clear()
        self.scene_.clear()
        self.view_.viewport().update()

    def __AddItem__(self, item):
        self.scene_.addItem(item)

    def AddNode(self, node_id, accepting=False, starting=False, tooltip=""):
        if node_id not in self.nodes_dict_:
            self.nodes_dict_[node_id] = graph_view.node.Node(accepting, starting, tooltip)
            self.__AddItem__(self.nodes_dict_[node_id])

    def UpdateCurrent(self, node_id, current):
        self.nodes_dict_[node_id].UpdateCurrent(current)
        self.view_.viewport().update()

    def AddEdge(self, source_id, target_id, label=""):
        if source_id not in self.nodes_dict_:
            # TODO(mmikoda): Error messages
            pass
            # sys.stderr.write("Cannot find source node. You should add it with addNode before adding an edge.")
        if target_id not in self.nodes_dict_:
            pass
            # sys.stderr.write("Cannot find target node. You should add it with addNode before adding an edge.")
        if (source_id, target_id) not in self.edges_dict_:
            paired = False
            if (target_id, source_id) in self.edges_dict_:
                paired = True
                self.edges_dict_[(target_id, source_id)].SetAsPaired()

            self.edges_dict_[(source_id, target_id)] = graph_view.edge.Edge(self.nodes_dict_[source_id],
                                                                            self.nodes_dict_[target_id],
                                                                            paired, label)

            self.__AddItem__(self.edges_dict_[(source_id, target_id)])
            self.nodes_dict_[source_id].AddEdge(self.edges_dict_[(source_id, target_id)])
            self.nodes_dict_[target_id].AddEdge(self.edges_dict_[(source_id, target_id)])

    def __Reorder__(self, layout, nodes_ids, edges_ids):
        if not nodes_ids:
            return
        positions = layout(nodes_ids, edges_ids)
        scene_rect = self.view_.mapToScene(self.rect()).boundingRect()
        for node_id in positions:
            (x, y) = positions[node_id]
            #TODO(violette):Thinks
            x /= 1.5
            y /= 1.5
            translation = PyQt5.QtCore.QPointF((scene_rect.width() - scene_rect.x() / 2) * x,
                                               (scene_rect.height() - scene_rect.y() / 2) * y)
            # x = x * scene_rect.width() - scene_rect.x()
            # y = y * scene_rect.height() - scene_rect.y()
            self.nodes_dict_[node_id].setPos(scene_rect.topLeft() + translation +
                                             PyQt5.QtCore.QPointF(graph_view.painting_supplier.FLAG_NODE_SIZE / 2,
                                                                  graph_view.painting_supplier.FLAG_NODE_SIZE / 2))
        for edge_id in edges_ids:
            self.edges_dict_[edge_id].Adjust()

    def Reorder(self, layout):
        self.__Reorder__(layout, self.nodes_dict_.keys(), self.edges_dict_.keys())

    def ReorderSelected(self, layout):
        nodes_ids = []
        for node_id in self.nodes_dict_:
            node = self.nodes_dict_[node_id]
            if node.isSelected():
                nodes_ids.append(node_id)
        self.__Reorder__(layout, nodes_ids, self.edges_dict_.keys())

    def GetSelectedStates(self):
        result = []
        for state in self.nodes_dict_:
            if self.nodes_dict_[state].isSelected():
                result.append(state)
        return result

    def AddSelectionChangedFunc(self, func):
        self.scene_.selectionChanged.connect(func)
