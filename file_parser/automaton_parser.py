import model.automaton_factory

__author__ = 'Violette'

import model.field
import model.automaton
import model.transition_function
from aux_functions import auxilary_functions
import functools

class Automaton_Parser:
    def __init__(self):
        self.automaton_factory = model.automaton_factory.AutomatonFactory()

    def Parse(self, automaton_dict, automaton_type):
        if automaton_type is model.automaton.AutomatonType.DFA:
            return self.__Parse__(automaton_dict, self.automaton_factory.CreateDFA)()
        if automaton_type is model.automaton.AutomatonType.NFA:
            return self.__Parse__(automaton_dict, self.automaton_factory.CreateNFA)()
        if automaton_type is model.automaton.AutomatonType.PDA:
            return self.__PdaAdditionalParse__(
                automaton_dict, self.__Parse__(automaton_dict, self.automaton_factory.CreatePDA))()

    def __PdaAdditionalParse__(self, automaton_dict, constructor):
        stack_alphabet = set(automaton_dict["Stack-Alphabet"])
        initial_stack_symbol = "#"
        if "Initial-Stack-Symbol" in automaton_dict:
            initial_stack_symbol = automaton_dict["Initial-Stack-Symbol"]
        return functools.partial(constructor, stack_alphabet, initial_stack_symbol)

    def __Parse__(self, automaton_dict, constructor):
        field_factory = model.field.FieldFactory()
        name = automaton_dict["Name"]
        alphabet = set(automaton_dict["Alphabet"])
        fields = []
        fields_infos = automaton_dict["States"]["Fields"]
        for field_info in fields_infos:
            field_factory.AddField(field_info["Name"], field_info["Values"])
            fields.append(field_factory.GetField(field_info["Name"]))
        transition_infos = automaton_dict["Transition"]
        transition = self.__ParseTransitionInfo__(transition_infos, field_factory)
        initial_state_infos = automaton_dict["Initial-state"]
        initial_state = {}
        for field_name in initial_state_infos:
            field = field_factory.GetField(field_name)
            initial_state[field] = initial_state_infos[field_name]
        accept_states_infos = automaton_dict["Accept-states"]
        accept_states_processed_infos = self.__ParseAcceptStatesInfo__(field_factory, accept_states_infos)
        accept_states = []
        for accept_state_info in accept_states_processed_infos:
            accept_state = auxilary_functions.GenerateStatesFromDescrpition(accept_state_info, fields)
            accept_states += accept_state
        return functools.partial(constructor, name, alphabet, fields, initial_state, accept_states, transition)

    def __ParseTransitionInfo__(self, transition_infos, field_factory):
        #TODO(violette): DO
        def aux(dict_elem):
            if isinstance(dict_elem,dict):
                result_dict = {}
                for key in dict_elem:
                    if key == "Alphabet":
                        field_info = "Alphabet"
                    elif key == "Stack-Alphabet":
                        field_info = "Stack-Alphabet"
                    else:
                        field_info = field_factory.GetField(key)
                    for value in dict_elem[key]:
                        result_dict[value] = aux(dict_elem[key][value])
                    return model.transition_function.TransitionFunctionClass.TransitionType.DictType(field_info, result_dict)
            return model.transition_function.TransitionFunctionClass.TransitionType.ValueType(dict_elem)

        result = {}
        for transition_info in transition_infos:
            if transition_info == "Stack":
                key = transition_info
                key_type = "Stack-Alphabet"
            else:
                key = field_factory.GetField(transition_info)
                key_type = key
            result[key] = {}
            result_dict = {}
            for value in transition_infos[transition_info]:
                if isinstance(transition_infos[transition_info][value], dict):
                    result_dict[value] = aux(transition_infos[transition_info][value])
                else:
                    result_dict[value] = model.transition_function.TransitionFunctionClass.TransitionType.ValueType(
                        transition_infos[transition_info][value])
            result[key] = model.transition_function.TransitionFunctionClass.TransitionType.DictType(key_type,
                                                                                                    result_dict)
        return result

    def __ParseAcceptStatesInfo__(self, field_factory, accept_states_infos):
        result = []
        for accept_state_info in accept_states_infos:
            dictionary = {}
            for field_name in accept_state_info:
                field = field_factory.GetField(field_name)
                value = accept_state_info[field_name]
                if not isinstance(value, list):
                    value = [value]
                dictionary[field] = value
            result.append(dictionary)
        return result