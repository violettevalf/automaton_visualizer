__author__ = 'Violette'
import file_parser.automaton_parser
import model.automaton
import json

class FileReader:
    def __init__(self):
        self.automaton_parser_ = file_parser.automaton_parser.Automaton_Parser()

    def Read(self, filename):
        file = open(filename)
        json_dict = json.loads(file.read())
        file.close()
        result = {}
        if "DFA" in json_dict:
            dfas_description = json_dict["DFA"]
            result.update(self.__CreateAutomata__(dfas_description, model.automaton.AutomatonType.DFA))
        if "NFA" in json_dict:
            nfas_description = json_dict["NFA"]
            result.update(self.__CreateAutomata__(nfas_description, model.automaton.AutomatonType.NFA))
        if "PDA" in json_dict:
            pdas_description = json_dict["PDA"]
            result.update(self.__CreateAutomata__(pdas_description, model.automaton.AutomatonType.PDA))
        return result

    def __CreateAutomata__(self, automata_description, automaton_type):
        result = {}
        if not isinstance(automata_description, list):
            automata_description = [automata_description]
        for automaton_description in automata_description:
            automaton = self.automaton_parser_.Parse(automaton_description, automaton_type)
            result[automaton.name_] = automaton
        return result