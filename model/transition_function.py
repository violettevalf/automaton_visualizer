__author__ = 'Violette'

import enum

from aux_functions import auxilary_functions


class TransitionFunctionClass:
    class TransitionType:
        class Type(enum.Enum):
            FIELD_VALUE = 1
            DICT = 2

        @staticmethod
        def ValueType(value):
            element = TransitionFunctionClass.TransitionType()
            element.type_ = TransitionFunctionClass.TransitionType.Type.FIELD_VALUE
            element.value_ = value
            return element

        @staticmethod
        def DictType(field_info, value):
            element = TransitionFunctionClass.TransitionType()
            element.type_ = TransitionFunctionClass.TransitionType.Type.DICT
            element.field_info_ = field_info
            element.value_ = value
            return element

    def __init__(self, dictionary, state_factory, alphabet, stack_alphabet=set()):
        self.alphabet_ = alphabet
        self.stack_alphabet_ = stack_alphabet
        self.dictionary_ = dictionary
        self.state_factory_ = state_factory

    def NextState(self, state, letter, stack_letter=None):
        new_fields = {}
        for field_info in state.fields_:
            new_field_value = self.__NextField__(state, letter, self.dictionary_[field_info], stack_letter)
            new_fields[field_info] = new_field_value
        states_dicts = auxilary_functions.GenerateStatesFromDescrpition(new_fields, self.state_factory_.field_infos_)
        if len(states_dicts) == 1:
            return self.state_factory_.GetState(new_fields)
        result = set()
        for states_dict in states_dicts:
            if not states_dict:
                continue
            result.add(self.state_factory_.GetState(states_dict))
        return result

    def NextStackWord(self, stack_letter, letter, state=None):
        return self.__NextField__(state, letter, self.dictionary_["Stack"], stack_letter)

    def AllPossibleNexts(self, state):
        result = {}
        for letter in self.alphabet_:
            if not self.stack_alphabet_:
                states = self.NextState(state, letter)
                if not states:
                    continue
                if not isinstance(states, set):
                    states = {states}
                states = list(states)
                if not states:
                    continue
                result[letter] = states
                continue
            for stack_letter in self.stack_alphabet_.union({"#"}):
                states = self.NextState(state, letter, stack_letter)
                if not isinstance(states, set):
                    states = {states}
                states = list(states)
                result[(letter, stack_letter)] = states
        return result

    def __NextField__(self, state, letter, changing_elem, stack_letter):
        if changing_elem.type_ is TransitionFunctionClass.TransitionType.Type.FIELD_VALUE:
            return changing_elem.value_
        if changing_elem.field_info_ == "Alphabet":
            if not letter in changing_elem.value_:
                return None
            return self.__NextField__(state, letter, changing_elem.value_[letter], stack_letter)
        if changing_elem.field_info_ == "Stack-Alphabet":
            return self.__NextField__(state, letter, changing_elem.value_[stack_letter], stack_letter)
        return self.__NextField__(state, letter, changing_elem.value_[state.fields_[changing_elem.field_info_]],
                                  stack_letter)

    def BuchiRun(self, current_state, word, inf_seq_start_symbol):
        if not isinstance(current_state, set):
            current_state = {current_state}
        while word[0] != inf_seq_start_symbol:
            new_states = set()
            for state in current_state:
                states = self.NextState(state, word[0])
                if not isinstance(states, set):
                    states = {states}
                new_states.update(states)
            word = word[1:]
            current_state = new_states
        pos = 0
        new_word = [inf_seq_start_symbol]
        for letter in word[1:]:
            new_word += [(letter, pos)]
            pos += 1
        word = new_word
        infinite_states = set()
        stack = []
        for state in current_state:
            stack += [(state, word, [])]
        while stack:
            current_state, word, history = stack.pop()
            if word[0] == inf_seq_start_symbol:
                word = word[1:] + word
            letter, pos = word[0]
            # TODO(violette) : Chyba dodaj warunek stopu, bo się chyba może nie zatrzymać, przemyśl
            if (current_state, pos) in history:
                should_add = False
                for state, state_pos in history:
                    if state == current_state and pos == state_pos:
                        should_add = True
                    if should_add:
                        infinite_states.add(state)
                continue
            history += [(current_state, pos)]
            new_states = self.NextState(current_state, letter)
            if not isinstance(new_states, set):
                new_states = {new_states}
            word = word[1:]
            for state in new_states:
                stack += [(state, word, list(history))]
        return infinite_states
