import model.automaton
from model.state import StateFactory
from model.transition_function import TransitionFunctionClass
import functools

__author__ = 'Violette'


class AutomatonFactory:
    def CreateDFA(self, name, alphabet, fields, initial_state_fields, accept_states_fields, transition_dict):
        """
        Function creates automaton from description.
        :param name: String
        :param alphabet: Set
        :param fields: List
        :param initial_state_fields: Dict
        :param accept_states_fields: List of dicts
        :param transition_dict: Dictionary from which TransitionFunctionClass will be created.
        :return: Complete automaton.
        """
        factory = StateFactory(fields)
        states = factory.States()
        initial_state = factory.GetState(initial_state_fields)
        accept_states = set()
        for accept_fields in accept_states_fields:
            accept_states.add(factory.GetState(accept_fields))
        transition_function_object = TransitionFunctionClass(transition_dict, factory, alphabet)
        return model.automaton.DFA(name, alphabet, fields, states, initial_state, accept_states,
                                   transition_function_object)

    def CreateNFA(self, name, alphabet, fields, initial_state_fields, accept_states_fields, transition_dict):
        """
        Function creates automaton from description.
        :param name: String
        :param alphabet: Set
        :param fields: List
        :param initial_state_fields: Dict
        :param accept_states_fields: List of dicts
        :param transition_dict: Dictionary from which TransitionFunctionClass will be created.
        :return: Complete automaton.
        """
        factory = StateFactory(fields)
        states = factory.States()
        initial_state = factory.GetState(initial_state_fields)
        accept_states = set()
        for accept_fields in accept_states_fields:
            accept_states.add(factory.GetState(accept_fields))
        transition_function_object = TransitionFunctionClass(transition_dict, factory, alphabet)
        return model.automaton.NFA(name, alphabet, fields, states, initial_state, accept_states,
                                   transition_function_object)

    def CreatePDA(self, name, alphabet, fields, initial_state_fields, accept_states_fields, transition_dict,
                  stack_alphabet, initial_stack_symbol):
        """
        Function creates automaton from description.
        :param name: String
        :param alphabet: Set
        :param fields: List
        :param initial_state_fields: Dict
        :param accept_states_fields: List of dicts
        :param transition_dict: Dictionary from which TransitionFunctionClass will be created.
        :return: Complete automaton.
        """
        factory = StateFactory(fields)
        states = factory.States()
        initial_state = factory.GetState(initial_state_fields)
        accept_states = set()
        for accept_fields in accept_states_fields:
            accept_states.add(factory.GetState(accept_fields))
        transition_function_object = TransitionFunctionClass(transition_dict, factory, alphabet, stack_alphabet)
        return model.automaton.PDA(name, alphabet, fields, states, initial_state, accept_states,
                                   transition_function_object, stack_alphabet, initial_stack_symbol)

    def __JoinStackBinder__(self, automaton1, automaton2, constructor):
        if isinstance(automaton1, model.automaton.PDA) and isinstance(automaton2, model.automaton.PDA):
            return None
        if isinstance(automaton1, model.automaton.PDA):
            return functools.partial(constructor, automaton1.stack_alphabet_, automaton1.initial_stack_symbol_)
        return functools.partial(constructor, automaton2.stack_alphabet_, automaton2.initial_stack_symbol_)

    def Union(self, automaton1, automaton2):
        if isinstance(automaton1, model.automaton.PDA) or isinstance(automaton2, model.automaton.PDA):
            return self.__JoinStackBinder__(automaton1, automaton2,
                                            self.__Union__(automaton1, automaton2, model.automaton.PDA))()
        if isinstance(automaton1, model.automaton.NFA) or isinstance(automaton2, model.automaton.NFA):
            return self.__Union__(automaton1, automaton2, model.automaton.NFA)()
        return self.__Union__(automaton1, automaton2, model.automaton.DFA)()

    def Intersection(self, automaton1, automaton2):
        if isinstance(automaton1, model.automaton.PDA) or isinstance(automaton2, model.automaton.PDA):
            return self.__JoinStackBinder__(automaton1, automaton2,
                                            self.__Intersection__(automaton1, automaton2, model.automaton.PDA))()
        if isinstance(automaton1, model.automaton.NFA) or isinstance(automaton2, model.automaton.NFA):
            return self.__Intersection__(automaton1, automaton2, model.automaton.NFA)()
        return self.__Intersection__(automaton1, automaton2, model.automaton.DFA)()

    def __GetStackAlphabet__(self, automaton1, automaton2):
        if isinstance(automaton1, model.automaton.PDA):
            return automaton1.stack_alphabet_
        if isinstance(automaton2, model.automaton.PDA):
            return automaton2.stack_alphabet_


    def __Union__(self, automaton1, automaton2, constructor):
        name = automaton1.name_ + " || " + automaton2.name_
        alphabet = automaton1.alphabet_.union(automaton2.alphabet_)
        fields = automaton1.fields_ + automaton2.fields_
        factory = StateFactory(fields)
        states = factory.States()
        initial_state = factory.GetState(
            self.__GetFactoryKey__(automaton1.initial_state_.fields_, automaton2.initial_state_.fields_))
        accept_states = self.__CreateUnionStates__(automaton1, automaton2, factory)
        transition_fn_dict = automaton1.transition_fn_.dictionary_.copy()
        transition_fn_dict.update(automaton2.transition_fn_.dictionary_)
        transition_fn = TransitionFunctionClass(transition_fn_dict, factory, alphabet,
                                                self.__GetStackAlphabet__(automaton1, automaton2))
        return functools.partial(constructor, name, alphabet, fields, states, initial_state, accept_states,
                                 transition_fn)

    def __Intersection__(self, automaton1, automaton2, constructor):
        name = automaton1.name_ + " && " + automaton2.name_
        alphabet = automaton1.alphabet_.union(automaton2.alphabet_)
        fields = automaton1.fields_ + automaton2.fields_
        factory = StateFactory(fields)
        states = factory.States()
        initial_state = factory.GetState(
            self.__GetFactoryKey__(automaton1.initial_state_.fields_, automaton2.initial_state_.fields_))
        accept_states = self.__CreateIntersectionStates__(automaton1.accept_states_, automaton2.accept_states_, factory)
        transition_fn_dict = automaton1.transition_fn_.dictionary_.copy()
        transition_fn_dict.update(automaton2.transition_fn_.dictionary_)
        transition_fn = TransitionFunctionClass(transition_fn_dict, factory, alphabet,
                                                self.__GetStackAlphabet__(automaton1, automaton2))
        return functools.partial(constructor, name, alphabet, fields, states, initial_state, accept_states,
                                 transition_fn)

    def __CreateUnionStates__(self, automaton1, automaton2, factory):
        result = set()
        for state1 in automaton1.accept_states_:
            for state2 in automaton2.states_:
                result.add(factory.GetState(self.__GetFactoryKey__(state1.fields_, state2.fields_)))
        for state1 in automaton1.states_:
            for state2 in automaton2.accept_states_:
                result.add(factory.GetState(self.__GetFactoryKey__(state1.fields_, state2.fields_)))
        return result

    def __CreateIntersectionStates__(self, accept_states1, accept_states2, factory):
        result = set()
        for state1 in accept_states1:
            for state2 in accept_states2:
                result.add(factory.GetState(self.__GetFactoryKey__(state1.fields_, state2.fields_)))
        return result

    def __GetFactoryKey__(self, fields1, fields2):
        res = fields1.copy()
        res.update(fields2)
        return res
