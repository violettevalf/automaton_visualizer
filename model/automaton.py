__author__ = 'Violette'

import enum

class AutomatonType(enum.Enum):
    DFA = 1
    NFA = 2
    PDA = 3

class Automaton:
    def __init__(self, name, alphabet, fields, states, initial_state, accept_states, transition_fn):
        # string
        self.name_ = name
        # set
        self.alphabet_ = alphabet
        # list
        self.fields_ = fields
        # set TODO(violette): Maybe list?
        self.states_ = states
        # states
        self.initial_state_ = initial_state
        # set
        self.accept_states_ = accept_states
        # TransitionFunctionClass
        self.transition_fn_ = transition_fn
        self.type_ = None
        self.current_state_ = None

    def IsAccepting(self, state):
        return state in self.accept_states_

    def Accepting(self):
        return False

    def Next(self, letter):
        pass

    def GetStack(self, state):
        return []

    def Type(self):
        return ""

    def AcceptDescription(self):
        return "Accepting: " + str(self.Accepting())

    def GetCurrent(self):
        return None

    def __str__(self):
        result = "Automaton: " + self.name_ + "\n"
        result += "Type: " + self.Type() + "\n"
        result += "Alphabet: " + str(self.alphabet_) + "\n"
        result += "Number of states: " + str(len(self.states_ )) + "\n"
        return result

class DFA(Automaton):
    def __init__(self, name, alphabet, fields, states, initial_state, accept_states, transition_fn):
        super(DFA, self).__init__(name, alphabet, fields, states, initial_state, accept_states, transition_fn)
        self.current_state_ = initial_state
        self.type_ = AutomatonType.DFA

    def Next(self, letter):
        self.current_state_ = self.transition_fn_.NextState(self.current_state_, letter)

    def IsCurrent(self, state):
        return self.current_state_ == state

    def Accepting(self):
        return self.current_state_ in self.accept_states_

    def Reset(self):
        self.current_state_ = self.initial_state_

    def Type(self):
        return "DFA"

    def GetCurrent(self):
        return [self.current_state_]


class NFA(Automaton):
    def __init__(self, name, alphabet, fields, states, initial_state, accept_states, transition_fn):
        super(NFA, self).__init__(name, alphabet, fields, states, initial_state, accept_states, transition_fn)
        self.current_state_ = {self.initial_state_}
        self.type_ = AutomatonType.NFA

    def Next(self, letter):
        result = set()
        for state in self.current_state_:
            states = self.transition_fn_.NextState(state, letter)
            if not states:
                continue
            if not isinstance(states, set):
                states = {states}
            result = result.union(states)
        self.current_state_ = result

    def IsCurrent(self, state):
        for cur_state in self.current_state_:
            if cur_state == state:
                return True
        return False

    def Accepting(self):
        result = False
        for state in self.current_state_:
            result = result or (state in self.accept_states_)
        return result

    def Reset(self):
        self.current_state_ = {self.initial_state_}

    def Type(self):
        return "NFA"

    def GetCurrent(self):
        return list(self.current_state_)

class PDA2(Automaton):
    def __init__(self, name, alphabet,  fields, states, initial_state, accept_states, transition_fn, stack_alphabet,
                 initial_stack_symbol):
        super(PDA, self).__init__(name, alphabet, fields, states, initial_state, accept_states, transition_fn)
        self.current_state_ = {self.initial_state_}
        self.type_ = AutomatonType.PDA
        self.stack_alphabet_ = stack_alphabet
        self.initial_stack_symbol_ = initial_stack_symbol
        # I add to end, and take from the end
        self.stack_ = [self.initial_stack_symbol_]

    def __GetStackTop__(self):
        result = self.stack_.pop()
        if result == self.initial_stack_symbol_:
            self.stack_.append(self.initial_stack_symbol_)
        return result

    def __AddToStack__(self, stack_letter):
        self.stack_.append(stack_letter)

    def Next(self, letter):
        result = set()
        stack_top = self.__GetStackTop__()
        for state in self.current_state_:
            states= self.transition_fn_.NextState(state, letter, stack_top)
            if not isinstance(states, set):
                states = {states}
            result = result.union(states)
        stack_word = self.transition_fn_.NextStackWord(stack_top, letter)
        for stack_letter in stack_word:
            self.__AddToStack__(stack_letter)
        self.current_state_ = result

    def IsCurrent(self, state):
        for cur_state in self.current_state_:
            if cur_state == state:
                return True
        return False

    def Accepting(self):
        result = False
        for state in self.current_state_:
            result = result or (state in self.accept_states_)
        return result

    def Reset(self):
        self.current_state_ = {self.initial_state_}
        self.stack_ = [self.initial_stack_symbol_]

    def GetStack(self):
        return self.stack_

    def Type(self):
        return "PDA"

class PDA(Automaton):
    def __init__(self, name, alphabet,  fields, states, initial_state, accept_states, transition_fn, stack_alphabet,
                 initial_stack_symbol):
        super(PDA, self).__init__(name, alphabet, fields, states, initial_state, accept_states, transition_fn)
        self.type_ = AutomatonType.PDA
        self.stack_alphabet_ = stack_alphabet
        self.initial_stack_symbol_ = initial_stack_symbol
        self.current_state_ = {(self.initial_state_, (self.initial_stack_symbol_,))}

    def __GetStackTop__(self, stack):
        result = stack.pop()
        if result == self.initial_stack_symbol_:
            stack.append(self.initial_stack_symbol_)
        return result

    def __AddToStack__(self, stack_letter, stack):
        stack.append(stack_letter)

    def Next(self, letter):
        result = set()
        for state, stack in self.current_state_:
            stack = list(stack)
            stack_top = self.__GetStackTop__(stack)
            states = self.transition_fn_.NextState(state, letter, stack_top)
            stack_word = self.transition_fn_.NextStackWord(stack_top, letter, state)
            for stack_letter in stack_word:
                self.__AddToStack__(stack_letter, stack)
            if not isinstance(states, set):
                states = {states}
            for new_state in states:
                result.add((new_state, tuple(stack)))
        self.current_state_ = result

    def IsCurrent(self, state):
        for cur_state, stack in self.current_state_:
            if cur_state == state:
                return True
        return False

    def Accepting(self):
        result = False
        for state, stack in self.current_state_:
            result = result or (state in self.accept_states_)
        return result

    def Reset(self):
        self.current_state_ = {(self.initial_state_,tuple([self.initial_stack_symbol_]))}

    def GetStack(self, for_state):
        result = []
        for state, stack in self.current_state_:
            if state == for_state:
                result += [stack]
        return result

    def Type(self):
        return "PDA"

    def GetCurrent(self):
        result = set()
        for state, stack in self.current_state_:
            result.add(state)
        return result
