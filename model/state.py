__author__ = 'Violette'

import model.field
import json

class State:
    def __init__(self, fields):
        self.fields_ = fields

    def Key(self):
        return str(self.fields_)

    def GetDescription(self):
        result = ""
        for field in self.fields_:
            result += str(field) + ": " + self.fields_[field] + "\n"
        return result

class StateFactory:
    def __init__(self, field_infos):
        self.field_infos_ = field_infos
        initializing_dicts = self.__CreateInitializingDictsRecursively__(0)
        self.states_ = {}
        for dic in initializing_dicts:
            key = self.GetKey(dic)
            self.states_[key] = State(dic)

    def GetKey(self, dictionary):
        return frozenset(dictionary.items())

    def GetState(self, initialiazing_dict):
        key = self.GetKey(initialiazing_dict)
        if not key in self.states_:
            return None
        return self.states_[key]

    def States(self):
        result = set()
        for key in self.states_:
            result.add(self.states_[key])
        return result

    def __CreateInitializingDictsRecursively__(self, field_num):
        if self.field_infos_[field_num] == self.field_infos_[-1]:
            result = []
            for value in self.field_infos_[field_num].possible_values_:
                result.append({self.field_infos_[field_num]: value})
            return result
        previous_result = self.__CreateInitializingDictsRecursively__(field_num + 1)
        result = []
        for value in self.field_infos_[field_num].possible_values_:
            for dictionary in previous_result:
                new_dictionary = dictionary.copy()
                new_dictionary[self.field_infos_[field_num]] = value
                result += [new_dictionary]
        return result