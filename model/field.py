__author__ = 'Violette'

class FieldInfo:
    def __init__(self, name, possible_values):
        self.name_ = name
        self.possible_values_ = possible_values

    def __str__(self):
        return self.name_

class FieldFactory:
    def __init__(self):
        self.field_dict = {}

    def AddField(self, name, possible_values):
        self.field_dict[name] = FieldInfo(name, possible_values)

    def GetField(self, name):
        return self.field_dict[name]