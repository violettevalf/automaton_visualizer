from PyQt5.QtWidgets import QApplication

__author__ = 'Violette'

from view import main_window
import graph_view.layouts

app = QApplication([])
window = main_window.Window()

window.show()
window.graph_widget.Reorder(graph_view.layouts.Layout.LayoutsDict()["neato layout"])
app.exec_()
