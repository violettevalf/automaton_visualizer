# README #

This document contains help for installing dependecies for Automaton Visualizer

### What is this repository for? ###

* Quick summary

	This repository contains Automaton Visulizer, which can visualize DFAs, NFAs and PDAs.

### Dependencies that need to be installed ###
* PyQt5 
* NetworkX
* numpy (required by some NetworkX layouts)
* PyGraphviz (required by some NetworkX layouts)
* Graphv (required by PyGraphviz)

### How do I get set up? ###
To install all necessary dependencies just run script install_dependecies.sh.