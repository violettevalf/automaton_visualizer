import enum


class EmptinessChecker:
    class Color(enum.Enum):
        BLUE = 1
        RED = 2
        BLACK = 3

    def __init__(self, nodes, edges, accepted):
        self.nodes_ = nodes
        self.accepted_ = accepted
        self.edges_ = edges
        self.colors_ = {}
        self.accept_buchi_ = False
        self.visited_ = set()

    def Emptiness(self, state):
        def DFS(state):
            if state in self.accepted_:
                return True
            self.visited_.add(state)
            for target_state in self.edges_[state]:
                if target_state in self.visited_:
                    continue
                if DFS(target_state):
                    return True
            return False
        self.visited_ = set()
        return not DFS(state)



    def BuchiEmptiness(self, state):
        def RedDFS(state):
            for node in self.edges_[state]:
                if self.colors_[node] == EmptinessChecker.Color.BLACK:
                    self.accept_buchi_ = True
                    return True
                if self.colors_[node] == EmptinessChecker.Color.BLUE:
                    self.colors_[node] = EmptinessChecker.Color.RED
                    RedDFS(node)
            return False
        def BlueDFS(state):
            allred = True
            self.colors_[state] = EmptinessChecker.Color.BLACK
            for node in self.edges_[state]:
                if node not in self.colors_:
                    if BlueDFS(node):
                        self.accept_buchi_ = True
                        return True
                elif self.colors_[node] == EmptinessChecker.Color.BLACK and (
                        node in self.accepted_ or state in self.accepted_):
                    self.accept_buchi_ = True
                    return True
                if self.colors_[node] != EmptinessChecker.Color.RED:
                    allred = False
            if allred:
                self.colors_[state] = EmptinessChecker.Color.RED
            elif state in self.accepted_:
                if RedDFS(state):
                    self.accept_buchi_ = True
                    return True
                self.colors_[state] = EmptinessChecker.Color.RED
            else:
                self.colors_[state] = EmptinessChecker.Color.BLUE
            return False
        self.colors_ = {}
        return not BlueDFS(state)


