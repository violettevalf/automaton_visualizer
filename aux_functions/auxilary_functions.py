import model.automaton

__author__ = 'Violette'

def ParseList(to_string):
    def ParseTouple(tupl):
        if not isinstance(tupl, tuple):
            return str(tupl)
        return "(" + str(tupl[0]) + ", " + str(tupl[1]) + ")"

    if not isinstance(to_string, list):
        return ParseTouple(to_string)
    text = "["
    for letter in to_string[:-1]:
        text += ParseTouple(letter) + ", "
    text += ParseTouple(to_string[-1]) + "]"
    return str(text)

def GenerateStatesFromDescrpition(description_dict, fields):
    def aux(field_position):
        if field_position >= len(fields):
            return [{}]
        field = fields[field_position]
        possible_values = []
        if field in description_dict:
            possible_values = description_dict[field]
        else:
            possible_values = field.possible_values_
        previous_result = aux(field_position + 1)
        result = []
        if not isinstance(possible_values, list):
            possible_values = [possible_values]
        for value in possible_values:
            for dictionary in previous_result:
                new_dict = dictionary.copy()
                new_dict[field] = value
                result += [new_dict]
        return result
    return aux(0)


def __SetState__(automaton, state):
    if isinstance(automaton, model.automaton.DFA):
        automaton.current_state_ = state
    if isinstance(automaton, model.automaton.NFA):
        if not isinstance(state, set):
            state = {state}
        automaton.current_state_ = state


def __GetStatesList__(automaton, state):
    if isinstance(automaton, model.automaton.DFA):
        return [state]
    if isinstance(automaton, model.automaton.NFA):
        return state
    if isinstance(automaton, model.automaton.PDA):
        return state


def __UpdateStack__(automaton, stack):
    if isinstance(automaton, model.automaton.DFA):
        stack += [automaton.current_state_]
    if isinstance(automaton, model.automaton.NFA):
        stack += automaton.current_state_


def IsEmpty(automaton):
    old_state = automaton.current_state_
    __SetState__(automaton, automaton.initial_state_)
    if automaton.Accepting():
        __SetState__(automaton, old_state)
        return False
    alphabet = automaton.alphabet_
    visited_states = set()
    stack = [automaton.initial_state_]
    while len(stack) > 0:
        state = stack.pop()
        if not state in visited_states:
            visited_states.add(state)
            for letter in alphabet:
                __SetState__(automaton, state)
                automaton.Next(letter)
                if automaton.Accepting():
                    __SetState__(automaton, old_state)
                    return False
                __UpdateStack__(automaton, stack)
    __SetState__(automaton, old_state)
    return True


def DoesAcceptWord(automaton, word):
    old_state = automaton.current_state_
    __SetState__(automaton, automaton.initial_state_)
    for letter in word:
        automaton.Next(letter)
    result = automaton.Accepting()
    __SetState__(automaton, old_state)
    return result


def GetEdges(automaton, state):
    return automaton.transition_fn_.AllPossibleNexts(state)