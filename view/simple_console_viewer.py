__author__ = 'Violette'

class ConsoleViewer:
    def __init__(self, automata):
        self.automata_ = automata

    def ShowAutomaton(self):
        print("DFA: " + self.automata_.name_)
        print("States:")
        for state in self.automata_.states_:
            self.ShowState(state)
        print("Current state:")
        if isinstance(self.automata_.current_state_, set):
            for state in self.automata_.current_state_:
                self.ShowState(state)
        else:
            self.ShowState(self.automata_.current_state_)
        print("Accepting: " + str(self.automata_.Accepting()))

    def ShowState(self, state):
        res = "{"
        for field in state.fields_:
            res += str(field) + ": "
            res += str(state.fields_[field])
            res += ", "
        res += "}"
        print(res)
