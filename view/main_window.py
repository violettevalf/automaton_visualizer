import functools

import PyQt5.QtWidgets
from PyQt5.QtWidgets import QMainWindow, QFileDialog

import controler.controler
import file_parser.file_reader
import graph_view.layouts
from ui_view.ui_main_window import Ui_MainWindow
from view import automaton_selection_dialog


class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.setupUi(self)
        self.run_button.clicked.connect(self.RunButtonClicked)
        self.one_step_button.clicked.connect(self.OneStepButtonClicked)
        self.step_by_step_button.clicked.connect(self.RunStepByStepButtonClicked)
        self.actionLoad.triggered.connect(self.LoadNewAutomaton)
        self.automaton_selection_combobox.currentTextChanged.connect(self.ChangeAutomaton)
        self.actionIntersection.triggered.connect(self.IntersectionClicked)
        self.actionUnion.triggered.connect(self.UnionClicked)
        layouts = graph_view.layouts.Layout.LayoutsDict()
        self.layout_ = layouts["neato layout"]
        self.AddLayoutsToMenu(layouts, self.ReorderAllClicked, self.menuReorder_all)
        self.AddLayoutsToMenu(layouts, self.ReorderSelectedClicked, self.menuReorded_selected)
        self.reset_button.clicked.connect(self.ResetClicked)
        self.inf_seq_entry.textChanged.connect(self.InfSeqStartSymbolChanged)

        self.file_reader_ = file_parser.file_reader.FileReader()
        self.automaton_dict_ = {}
        self.controler_ = controler.controler.Controller(self.graph_widget, self.automaton_description,
                                                         self.accept_decription, self.stack_list_view,
                                                         self.step_by_step_button, self.state_descr, self.stack_num,
                                                         self.Reorder, self.__AddNewAutomaton__)
        self.prev_stack_button.clicked.connect(self.controler_.PreviousStack)
        self.next_stack_button.clicked.connect(self.controler_.NextStack)
        self.LoadNewAutomaton()

    def AddLayoutsToMenu(self, layouts, reorder_func, menu):
        for layout_name in layouts:
            action_layout = PyQt5.QtWidgets.QAction(self)
            action_layout.setObjectName("action" + layout_name.split(" ")[0])
            action_layout.setText(layout_name)
            action_layout.triggered.connect(functools.partial(reorder_func, layouts[layout_name]))
            menu.addAction(action_layout)


    def AddControler(self, controler):
        self.controler_ = controler

    def RunButtonClicked(self):
        self.controler_.Run(self.entry.text())

    def OneStepButtonClicked(self):
        if not self.entry.text():
            return
        self.controler_.MakeOneStep(self.entry.text()[0])
        self.entry.setText(self.entry.text()[1:])

    def RunStepByStepButtonClicked(self):
        self.controler_.RunStepByStep(self.entry.text())

    def __ChooseAutomatonDict__(self):
        filename, _filter = QFileDialog.getOpenFileName(self, "Choose automaton description")
        if not filename:
            #filename = "examples/automaton-4.json"
            return
        return self.file_reader_.Read(filename)

    def __AddNewAutomaton__(self, automaton):
        self.automaton_dict_[automaton.name_] = automaton
        self.automaton_selection_combobox.clear()
        for key in self.automaton_dict_:
            self.automaton_selection_combobox.addItem(key)

    def LoadNewAutomaton(self):
        automaton_dict = self.__ChooseAutomatonDict__()
        if not automaton_dict:
            return
        self.automaton_dict_.update(automaton_dict)
        self.automaton_selection_combobox.clear()
        for key in self.automaton_dict_:
            self.automaton_selection_combobox.addItem(key)
        #self.ChangeAutomaton()

    def Reorder(self):
        self.graph_widget.Reorder(self.layout_)

    def ReorderAllClicked(self, layout):
        self.layout_ = layout
        self.graph_widget.Reorder(layout)

    def ReorderSelectedClicked(self, layout):
        self.graph_widget.ReorderSelected(layout)

    def ChangeAutomaton(self):
        if not self.automaton_selection_combobox:
            return
        automaton = self.automaton_dict_[self.automaton_selection_combobox.currentText()]
        self.controler_.ChangeAutomaton(automaton)
        self.graph_widget.Reorder(self.layout_)

    def IntersectionClicked(self):
        automaton = self.PickAutomaton()
        if not automaton:
            return
        self.controler_.IntersectionWith(automaton)

    def UnionClicked(self):
        automaton = self.PickAutomaton()
        if not automaton:
            return
        self.controler_.UnionWith(automaton)

    def ResetClicked(self):
        self.controler_.Reset()

    def PickAutomaton(self):
        dialog = automaton_selection_dialog.AutomatonSelectionDialog()
        for key in self.automaton_dict_:
            if self.controler_.CanJoinWith(self.automaton_dict_[key]):
                dialog.ui.automaton_selection.addItem(key)
        dialog.show()
        dialog.setFocus()
        if dialog.exec_():
            return self.automaton_dict_[dialog.ui.automaton_selection.currentText()]
        return None

    def InfSeqStartSymbolChanged(self):
        self.controler_.ChangeInfinitySeqStartSymbol(self.inf_seq_entry.text())
